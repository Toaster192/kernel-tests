#!/bin/bash
#
# Copyright (c) 2020 Red Hat, Inc. All rights reserved.
#
# This copyrighted material is made available to anyone wishing
# to use, modify, copy, or redistribute it subject to the terms
# and conditions of the GNU General Public License version 2.
#
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#

#
# This case just test raid modules loading and unloading for several times.
#

source ../../../../cki_lib/libcki.sh || exit 1  

function clean_all_loop_md
{
    umount /dev/md?                                                             
    mdadm --stop /dev/md?                                                       
    losetup -D                                                                  
    rm -f /opt/loop.* 
}

function startup
{
    clean_all_loop_md 
}

function cleanup
{
    clean_all_loop_md 
}

function runtest
{
    typeset i
    for i in 0 1 456 10; do
        cki_run_cmd_neu "modprobe -r raid$i"
        sleep 5
        cki_run_cmd_pos "modprobe raid$i"
        sleep 5
        cki_run_cmd_pos "modprobe -r raid$i"
        sleep 5
        cki_run_cmd_pos "modprobe raid$i"
        sleep 5
        cki_run_cmd_pos "modprobe -r raid$i"
    done
}

cki_main
exit $?
